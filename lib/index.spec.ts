import { expect } from 'chai'
import { Serializable, serialize, serializeAs, serializeFn } from './index'
import _zip from 'lodash.zip'

describe('Serializable e2e tests', () => {
  it('serializes string properties', () => {
    class Test extends Serializable {
      @serialize foo = 'foo'
    }
    expect(new Test().toJSON()).to.deep.equal({ foo: 'foo' })
  })

  it('serializes number properties', () => {
    class Test extends Serializable {
      @serialize foo = 3
    }
    expect(new Test().toJSON()).to.deep.equal({ foo: 3 })
  })

  it('serializes arrays', () => {
    class Test extends Serializable {
      @serialize foo = [3, 'four', 5]
    }
    expect(new Test().toJSON()).to.deep.equal({
      foo: [3, 'four', 5],
    })
  })

  it('can serialize changing names', () => {
    class Test extends Serializable {
      @serializeAs('foo-bar') fooBar = 'baz'
    }
    expect(new Test().toJSON()).to.deep.equal({ 'foo-bar': 'baz' })
  })

  it('can serialize other Serializables', () => {
    class Inner extends Serializable {
      @serialize num: number
      @serialize str: string
      constructor(num: number, str: string) {
        super()
        this.num = num
        this.str = str
      }
    }

    class Outer extends Serializable {
      @serialize list: Inner[]
      constructor(nums: number[], strs: string[]) {
        super()
        this.list = _zip(nums, strs).map(
          ([n, s]) => new Inner(n as number, s as string)
        )
      }
    }

    const obj = new Outer([1, 2, 3], ['a', 'b', 'c']).toJSON()
    expect(obj).to.deep.equal({
      list: [
        { num: 1, str: 'a' },
        { num: 2, str: 'b' },
        { num: 3, str: 'c' },
      ],
    })
  })

  it('can use a custom serializeFn', () => {
    class Test extends Serializable {
      @serializeFn(
        x => `[${x}]`,
        str => str.substring(1, str.length - 1)
      )
      str = 'wow'
    }
    expect(new Test().toJSON()).to.deep.equal({
      str: '[wow]',
    })
  })
})
