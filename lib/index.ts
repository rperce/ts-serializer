import 'reflect-metadata'
import _isArrayLikeObject from 'lodash.isarraylikeobject'

export type JSONValue = number | string | JSONObject | undefined
export type JSONObject = {
  [key: string]: JSONValue | JSONValue[]
}

/* eslint-disable @typescript-eslint/no-explicit-any */
function isArray(obj: any): boolean {
  return _isArrayLikeObject(obj)
}

export class UnserializableTypeError extends Error {}

export class Serializable {
  [key: string]: any
  private jsonify(val: any): JSONValue {
    if (typeof val == 'string') {
      return val as string
    } else if (typeof val == 'number') {
      return val as number
    } else if (val instanceof Serializable) {
      return (val as Serializable).toJSON()
    } else if (isArray(val)) {
      return val.map((each: any) => this.jsonify(each))
    }
    throw new UnserializableTypeError(
      'Value must be string, number, Serializable, or an array of those'
    )
  }

  private serializablePropertyKeys(): string[] {
    return Object.keys(this).filter((propName: string) => {
      return Reflect.hasMetadata('jsonName', this, propName)
    })
  }

  public toJSON(): JSONObject {
    const out: JSONObject = {}
    this.serializablePropertyKeys().forEach((propName: string) => {
      const key = Reflect.getMetadata('jsonName', this, propName)
      const val = this[propName]
      if (Reflect.hasMetadata('jsonFnTo', this, propName)) {
        const fn = Reflect.getMetadata('jsonFnTo', this, propName)
        out[key] = fn(val)
      } else {
        out[key] = this.jsonify(val)
      }
    })

    return out
  }

  public loadJSON(obj: JSONObject): void {
    this.serializablePropertyKeys().forEach((propName: string) => {
      const key = Reflect.getMetadata('jsonName', this, propName)
      const val = obj[key]
      if (Reflect.hasMetadata('jsonFnFrom', this, propName)) {
        const fn = Reflect.getMetadata('jsonFnFrom', this, propName)
        this[propName] = fn(val)
      } else {
        this[propName] = val
      }
    })
  }
}

/* Decorators */

export function serializeAs(jsonName: string) {
  return function(proto: Serializable, propName: string): void {
    Reflect.defineMetadata('jsonName', jsonName, proto, propName)
  }
}

export function serialize(proto: Serializable, propName: string): void {
  serializeAs(propName)(proto, propName)
}

export function serializeFn(
  fnTo: (arg0: any) => string,
  fnFrom: (arg0: string) => any,
  jsonName?: string | undefined
) {
  return function(proto: Serializable, propName: string): void {
    Reflect.defineMetadata('jsonFnTo', fnTo, proto, propName)
    Reflect.defineMetadata('jsonFnFrom', fnFrom, proto, propName)
    serializeAs(jsonName || propName)(proto, propName)
  }
}
